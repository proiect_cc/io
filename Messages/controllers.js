const express = require('express');
const nodemailer = require('nodemailer')
// const jwt = require('jsonwebtoken');
const MessagesService = require('./services.js');
const UsersService = require('../Users/services.js');
const {
    Users
} = require('../data');


const router = express.Router();

const SECRET = 'uweyrvbigefwefi37461374571354jskdhfhgfvjhwgf'
const SECRET_2 = 'aoiuyetcbguayisehfiAHsdsflkajsbcfhaf238762;;Q;;owr'
const EMAIL_SECRET = 'qwertyuioasdfghjklzxcvbnm,.2345sdfghwertyuio3456789xdcfg'
const transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'diana.brodoceanu.97@gmail.com',
    pass: 'WalzerOpus69NR.2'
  }
})
// //post new message
router.post('/', async (req, res, next) => {
    const {
        authorId,
        message,
        resolved,
        faq
    } = req.body;

    try {
        await MessagesService.add(authorId, message, resolved, faq);
        res.status(201).json({check:'OK'}).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

//get all messages
router.get('/', async (req, res, next) => {
    try {
        const messages = await MessagesService.getAll();
        res.json(messages);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

//get message by id
router.get('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        const message = await MessagesService.getById(id);
        res.json(message);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});


router.put('/getemail/:id', async (req, res, next) => {
    const {
        id
    } = req.params;
    const {
        authorId,
        message,
        resolved,
        faq,
    } = req.body;
    try {
      const user = await UsersService.getById(id);
      res.json(user).end();
  } catch (err) {
      // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
      next(err);
  }
});

// modify message by message_id
router.put('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;
    const {
        authorId,
        message,
        resolved,
        faq,
    } = req.body;
    try {
      await MessagesService.updateById(id, authorId, message, resolved, faq);
      res.status(204).json({check:'OK'}).end();
  } catch (err) {
      // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
      next(err);
  }
});

// delete message by message_id
router.delete('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;

    try {
        await MessagesService.deleteById(id);
        res.status(204).json({check:'OK'}).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

// delete all recipes from db
router.delete('/', async (req, res, next) => {
  try {
    // do logic
    await MessagesService.deleteAll();
    return res.status(200).json({check:'OK'}).end();
  } catch (err) {
    // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
    next(err);
  }
});

module.exports = router;
