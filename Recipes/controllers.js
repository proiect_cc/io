const express = require('express');

const RecipesService = require('./services.js');

const router = express.Router();

router.post('/', async (req, res, next) => {
  console.log("..IO service received request")
  const {
    name,
    clientId,
    goals,
    hairType,
    hairStructure,
    scalpMoisture,
    siliconFree,
    fragrance,
    shampooColor,
    conditionerColor,
    size,
    frequency,
    subscription
  } = req.body;

  // validare de campuri
  try {
    await RecipesService.add(
      name,
      clientId,
      goals,
      hairType,
      hairStructure,
      scalpMoisture,
      siliconFree,
      fragrance,
      shampooColor,
      conditionerColor,
      size,
      frequency,
      subscription
    );

    res.status(201).end();
  } catch (err) {
    if (err.message.includes('is not a valid enum value')) {
      next(new ServerError(`Wrong goal selection!`, 400));
    }
    next(err);
  }
});

// get all recipes
router.get('/', async (req, res, next) => {
  console.log("..IO service received request")
  try {
    const recipes = await RecipesService.getAll();
    res.json(recipes);
  } catch (err) {
    // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
    next(err);
  }
});

// get recipe by recipe_id
router.get('/:id', async (req, res, next) => {
  console.log("..IO service received request")
  const {
    id
  } = req.params;
  try {
    const recipe = await RecipesService.getById(id);
    res.json(recipe);
  } catch (err) {
    // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
    next(err);
  }
});

// get all recipes of user by user_id
router.get('/users/:id', async (req, res, next) => {
  console.log("..IO service received request")
  const {
    id
  } = req.params;
  try {
    console.log("before sending to mongoose Recipesservices")
    const recipes = await RecipesService.getByUserId(id);
    console.log(recipes)
    res.json(recipes);
  } catch (err) {
    // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
    console.log(err)
    next(err);
  }
});

// modify recipe by recipe_id
router.put('/:id', async (req, res, next) => {
  console.log("..IO service received request")
  const {
    id
  } = req.params;
  const {
    name,
    clientId,
    goals,
    hairType,
    hairStructure,
    scalpMoisture,
    siliconFree,
    fragrance,
    shampooColor,
    conditionerColor,
    size,
    frequency,
    subscription
  } = req.body;

  try {
    await RecipesService.updateById(
      id,
      name,
      clientId,
      goals,
      hairType,
      hairStructure,
      scalpMoisture,
      siliconFree,
      fragrance,
      shampooColor,
      conditionerColor,
      size,
      frequency,
      subscription
    );
    res.status(204).end();
  } catch (err) {
    // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
    next(err);
  }
});

// delete recipe by recipe_id
router.delete('/:id', async (req, res, next) => {
  console.log("..IO service received request")
  const {
    id
  } = req.params;

  try {
    await RecipesService.deleteById(id);
    res.status(204).end();
  } catch (err) {
    // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
    next(err);
  }
});

// delete all recipes from db
router.delete('/', async (req, res, next) => {
  console.log("..IO service received request")
  try {
    await RecipesService.deleteAll();
    return res.status(200).end();
  } catch (err) {
    // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
    next(err);
  }
});

module.exports = router;
