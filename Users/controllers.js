const express = require('express');

const RecipesService = require('../Recipes/services.js');
const UsersService = require('./services.js');

const router = express.Router();

router.post('/register', async (req, res, next) => {
    const {
        username,
        password,
        email,
        address,
        phoneNumber,
        firstName,
        lastName,
        hashedPassword
    } = req.body;

    try {
        await UsersService.add(username, password, email, address, phoneNumber, firstName, lastName);
        const id = await UsersService.getIdByUserName(username)
        res.json(id).status(201).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.post('/login', async (req, res, next) => {
  const {
      username,
      password
  } = req.body;

  try {
    const user = await UsersService.authenticate(username, password);
    res.json(user).status(200);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.get('/confirmation/:id', async (req, res, next) => {
  try{
    // const jwtOutput = jwt.verify(req.params.token, EMAIL_SECRET)
    // const { user: {id} } = jwtOutput
    //console.log(`########### print {id} => ${req.headers.userID}`)
    const id = req.params.id
    const confirmed = 'true'
    await UsersService.updateByUserId(id, confirmed)
    console.log("after mongo update")
    res.status(200).json({check: 'OK'})
  }catch(err){
    next(err);
  }
});

// get all users
router.get('/', async (req, res, next) => {
    try {
        const users = await UsersService.getAll();
        res.json(users);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
  const {
    id
  } = req.params;
  try {
    const user = await UsersService.getById(id);
    res.json(user);
  } catch (err) {
    // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
    next(err);
  }
});

router.put('/:id', async (req, res, next) => {
  const {
    id
  } = req.params;
  const {
      username,
      password,
      email,
      address,
      phoneNumber,
      firstName,
      lastName,
  } = req.body;

  // validare de campuri
  try {

    await UsersService.updateById(id, username, password, email, address, phoneNumber, firstName, lastName);
    res.status(204).end();
  } catch (err) {
    // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
    next(err);
  }
});
//delete user by id
router.delete('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;

    try {
        await UsersService.deleteById(id);
        await RecipesService.deleteRecipesByUserId(id)
        res.status(204).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.delete('/', async (req, res, next) => {
  try {
      // do logic
      await UsersService.deleteAll();
      return res.status(200).end();
  } catch (err) {
      // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
      next(err);
  }
});

module.exports = router;
