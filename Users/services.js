const {
    Users
} = require('../data');

const add = async (username, password, email, address, phoneNumber, firstName, lastName) => {
    // const hashedPassword = await hash(password);
    const role = username === 'admin' ? 'admin' : (username ==='suport' ? 'suport' : 'user');
    const confirmed = 'true'
    const user = new Users({
        username,
        password,
        role,
        email,
        address,
        phoneNumber,
        firstName,
        lastName,
        confirmed
    });
    await user.save();
};

const getIdByUserName = async (username) => {
  const userWithIdList = await Users.find({username: username})

  const [{
    _id,
    password,
    role,
    email,
    address,
    phoneNumber,
    firstName,
    lastName,
    confirmed
  }] = userWithIdList;
  const userWithId = userWithIdList[0]
  // console.log(`%%%%%%%%%%%%%%%%%%%%%% userWithId = ${userWithId}    ..in services.js`)
  // console.log(userWithId)
  // console.log(`%%%%%%%%%%%%%%%%%%%%%% _id = ${userWithId._id}    ..in services.js`)
  return userWithId._id
}

const getById = async (id) => {
  console.log("in getById function in services")
  const user = await Users.findOne({_id: id})

  const {
    username,
    password,
    role,
    email,
    address,
    phoneNumber,
    firstName,
    lastName,
    confirmed
  } = user;
  // console.log("in getById function in services")
  // console.log(`%%%%%%%%%%%%%%%%%%%%%% user = ${user}    ..in services.js`)
  return user
}

const authenticate = async (username, password) => {

    const userName = await Users.findOne({ username: username });
    const userEmail = await Users.findOne({ email: username })
    const user = userName == null ? userEmail : userName
    if (user === null) {
        throw new ServerError(`Utilizatorul inregistrat cu ${username} nu exista!`, 404);
    }
    return user
};

const getAll = async () => {
    return await Users.find();
};

//// confirmarea contului dupa register
const updateByUserId = async (id, newConfirmed) => {
    // console.log(`%%%%%%%%%%%%%%%% id to update for = ${id}   ..in updateByUserId`)
    // console.log(`%%%%%%%%%%%%%%%% new confirmed = ${newConfirmed}   ..in updateByUserId`)
    await Users.findByIdAndUpdate(id, {
        confirmed : newConfirmed
    });
};

const updateById = async (id, username, password, email, address, phoneNumber, firstName, lastName) => {
    await Users.findByIdAndUpdate(id, {
        username,
        password,
        email,
        address,
        phoneNumber,
        firstName,
        lastName
    });
};

const deleteById = async (id) => {
  try {
    await Users.findByIdAndDelete(id);
  }catch(err) {
    throw new ServerError(`Nu am strers userul`, 404);
  }
};

const deleteAll = async () => {
    // delete by id
    try{
        await Users.remove();
    } catch(err) {
        throw new ServerError(`Nu am strers userii`, 404);
    }

};

module.exports = {
    add,
    authenticate,
    getAll,
    deleteById,
    deleteAll,
    getIdByUserName,
    updateById,
    updateByUserId,
    getById

}
